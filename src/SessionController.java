/*
    + - Сделано; непротестированно,
    # - Сделано; протестированно,
    * - Частично,
    - - Не сделано,
    % - Переделываем,
    x или "пусто" - Не нужно
    
    TODO:                                       | SC  | DB  | UI |           
    ____________________________________________|_____|_____|____|______________
                                                |  *  |  *  |  * |
    ____________________________________________|_____|_____|____|______________
    Упраление пользователем:                    |  #  |  #  |  # |
        Вход:                                   |  #  |  #  |  # |
        Выход:                                  |  #  |  #  |  # |
    ____________________________________________|_____|_____|____|______________
    Справочники:                                |  #  |  #  |  # |
        Просмотр агентов:                       |  #  |  #  |  # |
        Просмотр поставляемых товаров:          |  #  |  #  |  # |
        Просмотр товаров:                       |  #  |  #  |  # | 
        Добавление агентов:                     |  #  |  #  |  # |
        Добавление поставляемых товаров:        |  #  |  #  |  # |
        Добавление товаров:                     |  #  |  #  |  # |
        Изменение агентов:                      |  #  |  #  |  # | Без удаления
        Изменение поставляемых товаров:         |  #  |  #  |  # | Без удаления
        Изменение товаров:                      |  #  |  #  |  # | Без удаления
    ____________________________________________|_____|_____|____|______________
    Заявки:                                     |  #  |  #  |  # |
        Просмотр заявок:                        |  #  |  #  |  # |
        Добавление заявок:                      |  #  |  #  |  # |
        Удаление заявок:                        |  #  |  #  |  # |
        Выполнение заявок:                      |  #  |  #  |  # |
    ____________________________________________|_____|_____|____|______________
    Отчёты:                                     |  #  |  #  |  - |
        Отчёт по заявкам:                       |  #  |  #  |  - |
    ____________________________________________|_____|_____|____|______________
    Проверка прав:                              |  x  |  #  |  x |
    Обработка ошибок:                           |  x  |  #  |  # |
    Логирование:                                |  x  |  #  |  x |
        Вход пользователя:                      |     |  #  |    |
        Выход пользователя:                     |     |  #  |    |
    ____________________________________________|_____|_____|____|______________
    Проектирование БД:                          |  x  |  #  |  x |
        Ограничения:                            |     |  #  |    |
        Резервирование:                         |     |  #  |    |
    ____________________________________________________________________________

*/

import Exceptions.NotLoggedInException;
import java.io.*;
import java.math.BigDecimal;
import java.sql.*;
import java.util.*;

public class SessionController 
{
    private long _sessionId = 0;
    private Properties pr = new Properties();
    private Connection _connection = null;
    
    public boolean isLogged()
    {
        return _sessionId != 0 && _connection != null;
    }
    
    /*
    * Открывает новую сессию с базой данных. 
    */
    public void beginSession(String login, String password)
            throws SQLException, IOException
    {
        FileInputStream inp = new FileInputStream("database.prop");
        pr.load(inp);
        inp.close();
        
        String databaseURL=pr.getProperty("dbURL");
        String dbUser = "IT38";
        String dbPassword = "it38";
        
        String driverName = pr.getProperty("driver");
        
        CallableStatement s = null;
        // Сюда складываются результаты
        ResultSet rs = null;
        
        /* TODO: Подключение к базе данных и установка _sessionId */
        
        try
        {
            Class.forName(driverName);
            
            _connection = DriverManager.getConnection(databaseURL, dbUser, dbPassword);
            
            s = _connection.prepareCall("{ call Login_user_proc(?, ?) }");
            s.setString(1, login);
            s.setString(2, password);
            s.execute();
            
            rs = s.getResultSet();
            
            rs.next();
            _sessionId = rs.getLong("SESSION_ID_HASH");
            //_connection.commit();
            
            // TODO: Тут возвращаем и устанавливаем права
        }
        catch(ClassNotFoundException e){
            System.out.println("Fireberd JDBC driver not found");
        }
        catch(Exception e)
        {
            if (e instanceof SQLException)
                throw e;
        }
    }
    
    /*
    *   Закрывает сессию
    */
    public void endSession() 
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        CallableStatement s = null;
                
        s = _connection.prepareCall("{ call logout(?) }");
        s.setLong(1, _sessionId);
        s.execute();
        _connection.commit();

        _sessionId = 0;
        if (_connection != null)
        {
            _connection.close();
            s.close();
        }
    }

    public void addAgent(String name, String city, String phone) 
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        CallableStatement s = _connection.prepareCall("{ call user_add_agent(?, ?, ?, ?) }");
        s.setLong(1, _sessionId);
        s.setString(2, name);
        s.setString(3, city);
        s.setString(4, phone);

        s.execute();
    }

    public void addProd(String name, int measureId) 
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        
        // Тут проверка на возможность выполнения
        CallableStatement s = _connection.prepareCall("{ call user_add_prod(?, ?, ?) }");
        s.setLong(1, _sessionId);
        s.setString(2, name);
        s.setInt(3, measureId);

        s.execute();
    }
    
    // НЕПРАВИЛЬНО, ПЕРЕДЕЛАТЬ С ПОМОЩЬЮ _ADD_OPERATION
    public void addProdWh(String prodName, String whName, String agName, 
            BigDecimal price, java.sql.Date postDate, int amount) 
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        CallableStatement s = _connection.prepareCall("{ call user_add_prod_wh(?, ?, ?, ?, ?, ?, ?) }");
        s.setLong(1, _sessionId);
        s.setString(2, whName);
        s.setString(3, prodName);
        s.setInt(4, amount);
        s.setString(5, agName);
        s.setBigDecimal(6, price);
        s.setDate(6, postDate);
        
        s.execute();
    }
    
    public void addRequest(String whName, String prodName, String agentName, 
            int amount, BigDecimal price) 
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        CallableStatement s = 
                _connection.prepareCall("{ call user_add_request(?, ?, ?, ?, ?, ?) }");
        s.setLong(1, _sessionId);
        s.setString(2, whName);
        s.setString(3, agentName);
        s.setString(4, prodName);
        s.setInt(5, amount);
        s.setBigDecimal(6, price);
        
        s.execute();
    }
    
    public void addAgentProduct(String agentName, String productName, int limit)
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        CallableStatement s =
                _connection.prepareCall("{ call user_add_agent_prod(?, ?, ?, ?) }");
        s.setLong(1, _sessionId);
        s.setString(2, agentName);
        s.setString(3, productName);
        s.setInt(4, limit);
        
        s.execute();
    }
    
    public List<String> getMeasures() 
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        
        List<String> names = new LinkedList<>();
        ResultSet rs = null;
        Statement s;
        
        s = _connection.createStatement();
        rs = s.executeQuery("SELECT * FROM GET_ALL_MEASURES");
        boolean canMove = rs.next();
        
        while(canMove)
        {
            names.add(rs.getString("RES"));
            canMove = rs.next();
        }
                
        return names;
    }
    
    public List<Agent> getAgents() 
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        List<Agent> agents = new LinkedList<>();
        ResultSet rs = null;
        PreparedStatement s;
        
        s = _connection.prepareStatement("select * from GET_AGENTS(?)");
        s.setLong(1, _sessionId);

        rs = s.executeQuery();
        boolean canMove = rs.next();
        
        while(canMove)
        {

            int id = rs.getInt("ID");
            String name = rs.getString("Name");
            String city = rs.getString("City");
            String phone = rs.getString("Phone");

            Agent ag = new Agent(id, name, city, phone);

            agents.add(ag);
            canMove = rs.next();
        }
                
        return agents;
    }
    
    public List<Product> getProducts()
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        List<Product> products = new LinkedList<>();
        ResultSet rs = null;
        PreparedStatement s;
        
        s = _connection.prepareStatement("select * from GET_PRODUCTS(?) ");
        s.setLong(1, _sessionId);

        rs = s.executeQuery();
        boolean canMove = rs.next();

        while(canMove)
        {
            int id = rs.getInt("ID");
            String name = rs.getString("Name");
            String measure = rs.getString("Measure");

            Product pr = new Product(id, name, measure);

            products.add(pr);
            canMove = rs.next();
        }
                
        return products;
    }
    
    public List<Warehouse> getWarehouses()
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        List<Warehouse> warehouses = new LinkedList<>();
        ResultSet rs = null;
        PreparedStatement s;
        
        s = _connection.prepareStatement("select * from GET_WAREHOUSES(?) ");
        s.setLong(1, _sessionId);

        rs = s.executeQuery();
        boolean canMove = rs.next();

        while(canMove)
        {
            int id = rs.getInt("ID");
            String name = rs.getString("Name");
            String town = rs.getString("TOWN");
            int max_amount = rs.getInt("MAX_AMOUNT");

            Warehouse pr = new Warehouse(id, name, town,  max_amount);

            warehouses.add(pr);
            canMove = rs.next();
        }
                
        return warehouses;
    }
    
    public List<AgentProdPair> getAgentsProducts()
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        List<AgentProdPair> products = new LinkedList<>();
        ResultSet rs = null;
        PreparedStatement s;
        
        s = _connection.prepareStatement("select * from GET_AGENT_PROD(?)");
        s.setLong(1, _sessionId);

        rs = s.executeQuery();
        boolean canMove = rs.next();

        while(canMove)
        {
            String agent = rs.getString("aname");
            String product = rs.getString("pname");
            int limit = rs.getInt("limit");
            
            AgentProdPair pr = new AgentProdPair(agent, product, limit);

            products.add(pr);
            canMove = rs.next();
        }
                
        return products;
    }
    
    public List<Request> getRequests() 
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        List<Request> request = new LinkedList<>();
        ResultSet rs = null;
        PreparedStatement s;
        
        s = _connection.prepareStatement("select * from GET_REQUESTS(?)");
        s.setLong(1, _sessionId);

        rs = s.executeQuery();
        boolean canMove = rs.next();

        while(canMove)
        {
            int id = rs.getInt("ID");
            String prodName = rs.getString("PROD_NAME");
            String agentName = rs.getString("AGENT_NAME");
            String whName = rs.getString("WH_NAME");
            int amount = rs.getInt("AMOUNT");
            BigDecimal price = rs.getBigDecimal("PRICE");
            
            Request req = new Request(id, prodName, agentName, whName, amount, price);
            
            request.add(req);
            canMove = rs.next();

        }
                
        return request;
    }
    
    public void completeRequest(int reqId) 
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        CallableStatement s = _connection.prepareCall("{ call user_complete_request(?, ?) }");
        s.setLong(1, _sessionId);
        s.setLong(2, reqId);
        
        s.execute();
    }

    public void EditProduct(int productId, String name, int measureId)
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        CallableStatement s = _connection.prepareCall("{ call user_edit_product(?, ?, ?, ?)}");
        s.setLong(1, _sessionId);
        s.setInt(2, productId);
        s.setString(3, name);
        s.setInt(4, measureId);
        
        s.execute();
    }
    
    public void EditAgent(int agentId, String name, String city, String phone)
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        CallableStatement s = _connection.prepareCall("{ call user_edit_agent(?, ?, ?, ?, ?)}");
        s.setLong(1, _sessionId);
        s.setInt(2, agentId);
        s.setString(3, name);
        s.setString(4, city);
        s.setString(5, phone);
        
        s.execute();
    }
    
    public void EditAgentProd(String agName, String prodName, int newLimit)
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        CallableStatement s = _connection.prepareCall("{ call USER_EDIT_AGENT_PROD(?, ?, ?, ?)}");
        s.setLong(1, _sessionId);
        s.setString(2, agName);
        s.setString(3, prodName);
        s.setInt(4, newLimit);
        
        s.execute();
    }
    
    public void CancelRequest(int id)
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        CallableStatement s = _connection.prepareCall("{ call USER_CANCEL_REQUEST(?, ?)}");
        s.setLong(1, _sessionId);
        s.setInt(2, id);
        
        s.execute();
    }
    
    public void makeSupply(String agent, String product, String warehouse, int amount, BigDecimal price, java.sql.Date postDate)
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        CallableStatement s = _connection.prepareCall("{ call USER_ADD_OPERATION(?, ?, ?, ?, ?, ?, ?)}");
        s.setLong(1, _sessionId);
        s.setString(2, agent);
        s.setString(3, product);
        s.setString(4, warehouse);
        s.setInt(5, amount);
        s.setBigDecimal(6, price);
        s.setDate(7, postDate);
        
        s.execute();
    }
    
    // Если productName == null, то отчёт совершается по всем типам товаров
    public List<ReportData> reportReq(String productName)
                throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        CallableStatement s = _connection.prepareCall("{ call REQUESTS_REPORT(?, ?)}");
        s.setLong(1, _sessionId);
        s.setString(2, productName);
        
        s.execute();
        
        ResultSet rs = s.executeQuery();
        boolean canMove = rs.next();
        List<ReportData> request = new LinkedList<>();

        while(canMove)
        {
            int id = rs.getInt("REQ_ID");
            String pname = rs.getString("PROD_NAME");
            String aname = rs.getString("AGENT_NAME");
            String wname = rs.getString("WH_NAME");
            int amount = rs.getInt("AMOUNT");
            int mon = rs.getInt("MON_TO_COMPLETE");
            
            ReportData req = new ReportData(id, pname, aname, wname, amount, mon);
            
            request.add(req);
            canMove = rs.next();
        }
                
        return request;
    }
    
    public String StringReport(String productName)
            throws SQLException, NotLoggedInException
    {
        if (!isLogged())
            throw new NotLoggedInException();
        CallableStatement s = _connection.prepareCall("{ call REQUESTS_REPORT(?, ?)}");
        s.setLong(1, _sessionId);
        s.setString(2, productName);
        
        s.execute();
        
        ResultSet rs = s.executeQuery();
        boolean canMove = rs.next();
        List<ReportData> request = new LinkedList<>();
        
        int l1 = 10, l2 = 6, l3 = 6, l4 = 6, l5 = 10, l6 = 10;
        
        while(canMove)
        {
            int id = rs.getInt("REQ_ID");
            String pname = rs.getString("PROD_NAME");
            String aname = rs.getString("AGENT_NAME");
            String wname = rs.getString("WH_NAME");
            int amount = rs.getInt("AMOUNT");
            int mon = rs.getInt("MON_TO_COMPLETE");
            
            ReportData req = new ReportData(id, pname, aname, wname, amount, mon);
            
            request.add(req);
            canMove = rs.next();
            
            if (pname != null && pname.length() > l2)
                l2 = pname.length();
            if (aname != null &&aname.length() > l3)
                l3 = aname.length();
            if (wname != null && wname.length() > l4)
                l4 = wname.length();
        }

        String result = ReportData.makeReportRow(request, l1, l2, l3, l4, l5, l6);
        
        return result;
    }
}
