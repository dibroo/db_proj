
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author darin_k0xwzcj
 */
public class ReportData {
    private int _reqId;
    private String _prodName;
    private String _agent;
    private String _warehouse;
    private int _amount;
    private int _monthToComplete;
    
    public ReportData(int id, String name, String agent, String wh, int amount, int month)
    {
        _reqId = id;
        _prodName = name;
        _agent = agent;
        _warehouse = wh;
        _amount = amount;
        _monthToComplete = month;
    }
    
    private static String formLen(String f, int len)
    {
        return "%" + (-len) + f;
    }
    
    // Создаёт строку таблицы отчёта, 
    // l1 - макс. длина ID,
    // l2 - макс. длина названия товара,
    // l3 - макс. длина имени агента,
    // l4 - макс. длина названия склада,
    // l5 - макс. длина количества товара 
    public String makeReportRow(int l1, int l2, int l3, int l4, int l5, int l6)
    {
        if (_prodName == null)
            return "";
        
        String fs = formLen("d", l1) + ";" + formLen("s", l2) + ";" +
                    formLen("s", l3) + ";" + formLen("s", l4) + ";" +
                    formLen("d", l5) + ";" + formLen("d", l6) + "%n";
        String result = String.format(fs, 
                _reqId, 
                _prodName, _agent, _warehouse, 
                _amount, _monthToComplete);
        
        return result;
    }
    
    public static String makeReportRow(List<ReportData> data, int l1, int l2, int l3, int l4, int l5, int l6)
    {
        String fs = formLen("s", l1) + ";" + formLen("s", l2) + ";" +
                    formLen("s", l3) + ";" + formLen("s", l4) + ";" +
                    formLen("s", l5) + ";" + formLen("s", l6) + "%n";
        
        String res = String.format(fs, 
                "ID", 
                "Товар", "Агент", "Склад", 
                "Кол-во", "Вып. через");
        
        for(ReportData r : data)
        {
            res += r.makeReportRow(l1, l2, l3, l4, l5, l6);
        }
        
        return res;
    }
}
