
import java.util.Vector;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Анатолий
 */
public class Warehouse {
    private int _id;
    private String _name;
    private String _city;
    private int _max_amount;
    
    public Warehouse(int id, String name, String city, int max_amount)
    {
        _id = id;
        _name = name;
        _city = city;
        _max_amount = max_amount;
    }
    
    public Vector<Object> get_all() {
        Vector<Object> vs = new Vector<Object>();
        vs.add(_id);
        vs.add(_name);
        vs.add(_city);
        vs.add(_max_amount);
        return vs;
    } 
}
