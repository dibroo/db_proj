
import java.sql.*;
import java.util.*;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author darin_k0xwzcj
 */
public class Agent
{
    private int _id;
    private String _name;
    private String _city;
    private String _phone;
    
    public Agent(int id, String name, String city, String phone)
    {
        _id = id;
        _name = name;
        _city = city;
        _phone = phone;
    }
    
    public Vector<Object> get_all() {
        Vector<Object> vs = new Vector<Object>();
        vs.add(_id);
        vs.add(_name);
        vs.add(_city);
        vs.add(_phone);
        return vs;
    }
}
