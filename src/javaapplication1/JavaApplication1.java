package javaapplication1;

import java.sql.*;
import java.util.*;
import java.io.*;
import java.math.BigDecimal;

public class JavaApplication1 {
    public static void main(String[] args) {
        String databaseURLFrom = "jdbc:firebirdsql:class.mmcs.sfedu.ru/3050:/fbdata/38mi/plan.fdb ?charset=\"UTF-8\"";
        String databaseURLTo = "jdbc:firebirdsql:class.mmcs.sfedu.ru/3050:/fbdata/38mi/abrgon.fdb ?charset=\"UTF-8\"";
        String user = "IT38";
        String password = "it38";
        String driverName = "org.firebirdsql.jdbc.FBDriver";
        
        Connection fromConn = null;
        Connection toConn = null;
        // CallableStatement s = _connection.prepareCall("{ call user_add_agent(?, ?, ?, ?) }");   
        Statement s = null;
        CallableStatement p = null;
        ResultSet rs = null;
        
        int id = 1;
        
        try
        {
            Class.forName(driverName);
            fromConn = DriverManager.getConnection(databaseURLFrom,user,password);   
            toConn = DriverManager.getConnection(databaseURLTo,user,password);
            
            s = fromConn.createStatement();
            rs = s.executeQuery("select * from OPERATION");
            
            p = toConn.prepareCall("{ call make_operation(?, ?, ?, ?, ?, ?) }");
            
            while (rs.next()) {
                String ag = rs.getString("ID_AG");
                String tv = rs.getString("ID_TOVAR");
                String wh = rs.getString("ID_WH");
                String tp = rs.getString("TYPEOP");
                int amnt = rs.getInt("KOL");
                BigDecimal pr = rs.getBigDecimal("PRICE");
                java.sql.Date date = rs.getDate("POST_DATE");
                
                p.setInt(1, oldId(ag));
                p.setInt(2, oldId(tv));
                p.setInt(3, oldId(wh));
                if (tp == "R")
                    amnt = -amnt;
                p.setInt(4, amnt);
                p.setBigDecimal(5, pr);
                p.setDate(6, date);
                
                p.execute();
            }
        }
        catch(ClassNotFoundException e){
            System.out.println("Fireberd JDBC driver not found");
        }
        catch(SQLException e){
            System.out.println("SQLException" + e.getMessage());
        }
        catch(Exception e) {
            System.out.println("Exception" + e.getMessage());
        }   
    }
    
    private static int oldId(String old)
    {
        return Integer.parseInt(old.substring(1).trim());
    }
}
