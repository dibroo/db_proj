
import java.sql.SQLException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author darin_k0xwzcj
 */
public class SQLExceptionHandler {    
    public static String GetMessage(SQLException e)
    {
        try
        {
            String code = e.getMessage().split(" |\n")[4].trim();
            //System.out.printf("%s", code);
       
            if ("50".equals(code))
                return "На складе недостаточно товара";
            if ("51".equals(code))
                return "Невозможно выполнить запрос, на складе недостаточно товара";
            if ("54".equals(code))
                return "Агент с таким именем уже существует";
            if ("56".equals(code))
                return "Товар с таким именем уже существует";
            if ("57".equals(code))
                return "Склад с таким именем уже существует";
            if ("69".equals(code))
                return "В данный момент агент не может поставить этот товар";
            if ("59".equals(code))
                return "На складе больше нет места";
            if ("60".equals(code))
                return "Минимальное количество товара в заявке: 300";
            if ("66".equals(code))
                return "Пользователь с таким именем не зарегестрирован в системе";
            if ("67".equals(code))
                return "Пользователь вышел из системы или кто-то вошёл под этим именем с другого устройства";
            if ("68".equals(code))
                return "У вас недостаточно прав для выполнения этой операции";
            if ("69".equals(code))
                return "Агент не поставляет такой товар";
            if ("70".equals(code))
                return "Неверное количество товара";
            if ("71".equals(code))
                return "Неверный пароль";
            if ("63".equals(code))
                return "Неизвестный агент";
            if ("64".equals(code))
                return "Неизвестный товар";
            if ("65".equals(code))
                return "Неизвестный склад";
        }
        catch (Exception exn)
        { 
        }
       return e.getMessage();
    }
    
    public static boolean empty_string(String s) {
        return s.trim().equals("");        
    }
}
