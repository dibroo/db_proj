
import java.util.Vector;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author darin_k0xwzcj
 */
public class Product 
{
    private int _id;
    private String _name;
    private String _measure;
    
    public Product(int id, String name, String measure)
    {
        _id = id;
        _name = name;
        _measure = measure;
    }
    
    public Vector<Object> get_all() {
        Vector<Object> vs = new Vector<Object>();
        vs.add(_id);
        vs.add(_name);
        vs.add(_measure);
        return vs;
    }
    
  
}
