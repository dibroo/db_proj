
import java.util.Vector;


public class AgentProdPair 
{
    private String _agent;
    private String _product;
    private int _limit;
    
    public AgentProdPair (String agent, String product, int limit)
    {
        _agent = agent;
        _product = product;
        _limit = limit;
    }
    
    public Vector<Object> get_all() {
        Vector<Object> vs = new Vector<Object>();
        vs.add(_agent);
        vs.add(_product);
        vs.add(_limit);
        return vs;
    }
}
