
import java.math.BigDecimal;
import java.util.Vector;

public class Request 
{
    int _id;
    String _prodName;
    String _agentName;
    String _whName;
    int _amount;
    BigDecimal _price;
    
    public Request (int id, String prodName, String agentName, String whName,
            int amount, BigDecimal price)
    {
        _id = id;
        _prodName = prodName;
        _agentName = agentName;
        _whName = whName;
        _amount = amount;
        _price = price;
    }
    
    public Vector<Object> get_all() {
        Vector<Object> vs = new Vector<Object>();
        vs.add(_id);
        vs.add(_prodName);
        vs.add(_agentName);
        vs.add(_whName);
        vs.add(_amount);
        vs.add(_price);
        return vs;
    }
}
